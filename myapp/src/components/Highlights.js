import { Row, Col, Card} from 'react-bootstrap'

export default function Highlights() {
	return(
		<Row>
			<Col xs={12} md={4}>
				<Card className= "cardHighlight p-3">
					<Card.Body>
						<Card.Title>
							<h2>Learn From Home</h2>
						</Card.Title>
						<Card.Text>
							Lorem ipsum dolor sit amet, consectetur adipisicing elit. Debitis consequuntur placeat quia facere nisi unde molestias expedita distinctio repellat. At blanditiis alias nobis fugiat nesciunt vero magnam neque hic minima.
						</Card.Text>
					</Card.Body>
				</Card>
			</Col>

			<Col xs={12} md={4}>
				<Card className= "cardHighlight p-3">
					<Card.Body>
						<Card.Title>
							<h2>Study Now, Pay Later</h2>
						</Card.Title>
						<Card.Text>
							Lorem ipsum dolor sit amet consectetur adipisicing elit. Ipsam nisi quod minima fugiat assumenda, debitis in beatae, similique voluptatibus cupiditate, consectetur doloribus libero, ab iure. Possimus totam labore officia at.
						</Card.Text>
					</Card.Body>
				</Card>
			</Col>

			<Col xs={12} md={4}>
				<Card className= "cardHighlight p-3">
					<Card.Body>
						<Card.Title>
							<h2>Be Part of our Community</h2>
						</Card.Title>
						<Card.Text>
							Lorem ipsum dolor sit, amet consectetur adipisicing elit. Adipisci obcaecati non doloremque nostrum eius, libero quidem nemo harum quaerat nulla quas, maiores enim a est vero omnis facilis itaque. Provident!
						</Card.Text>
					</Card.Body>
				</Card>
			</Col>
		</Row>
	)
}