import {useState, useEffect} from 'react'
import { Row, Col, Card,} from 'react-bootstrap'
import { Link } from 'react-router-dom'

export default function CourseCard({courseProp}) {

	console.log(courseProp)
	// console.log(typeof props)

	// deconstruct the courseProp into their own variables
	const {name, description, price, _id} = courseProp

	// Syntax
		// const [getter, setter] = useState(initialValueOfGetter)
	// const [count, setCount] = useState(0)
	// const [seats, seatCount] = useState(30)

	// function enroll(){
	// 	if (seats > 0) {
	// 		setCount(count + 1)
	// 		console.log('Enrollees' + ' ' + count)
	// 		seatCount(seats - 1)
	// 		console.log('Seats: ' + seats)
	// 	}
	// }

	// useEffect(() => {
	// 	if (seats === 0){
	// 		alert('No more seats available')
	// 	}
	// }, [seats])

	return (
		<Row>
			<Col xs={12} md={4}>
				<Card className="cardHighlight p-3 mt-4" >
					<Card.Body>
						<Card.Title>
							<h2>{name}</h2>
						</Card.Title>
						<Card.Text>
							Description:
						</Card.Text>
							<Card.Subtitle>{description}</Card.Subtitle>
						<Card.Text>
							Price:
						</Card.Text>
							<Card.Subtitle>{price}</Card.Subtitle>
						<Link className="btn btn-primary" to={`/courses/${_id}`}>Details</Link>
					</Card.Body>
				</Card>
			</Col>
		</Row>
	)
}