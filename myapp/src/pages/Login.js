import { Fragment, useState, useContext, useEffect } from 'react'
import { Navigate } from 'react-router-dom'
import { Form, Button } from 'react-bootstrap'
import Swal from 'sweetalert2'
import UserContext from '../UserContext'

export default function Login(){

	const { user, setUser } = useContext(UserContext)

	const [email, setEmail] = useState('')
	const [password1, setPassword1] = useState('')
	const [isActive, setIsActive] = useState(false)

	function loginUser(e){
		e.preventDefault()

		// fetch syntax:
			// fetch(url, {options})
			// .then(res => res.json())
			// .then(data => {})

		fetch('https://arcane-taiga-51348.herokuapp.com/users/login',
			{
				method: 'POST',
				headers: {
					'Content-Type' : 'application/json'
				},
				body: JSON.stringify({
					email: email,
					password: password1
				})
			}
		) 
		.then(res => res.json())
		.then(data => {
			console.log(data)

			if (typeof data.access !== 'undefined') {

				localStorage.setItem('token', data.access)
				retrieveUserDetails(data.access)

				Swal.fire({
					title: 'Login Successful',
					icon: 'success',
					text: 'Welcome to Zuitt!'
				})
			} else {

				Swal.fire({
					title: 'Authentication Failed',
					icon: 'error',
					text: 'Check details and try again'
				})
			}
		})

		// setItem
		// localStorage.setItem('email', email)
		// localStorage.setItem('key', value)

		// setUser({

		// 	email: localStorage.getItem('email')
		// })

		// Clear input fields
		setEmail('')
		setPassword1('')

		// alert('You are now logged in')
	}

	const retrieveUserDetails = (token) => {

		fetch('https://arcane-taiga-51348.herokuapp.com/users/details', {
			method: "POST",
			headers: {
				Authorization: `Bearer ${token}`
			}
		})
		.then(res => res.json())
		.then(data => {

			console.log(data)

			setUser({
				id: data._id,
				isAdmin: data.isAdmin
			})
		})
	}

	useEffect(() => {
		if ((email !== '' && password1 !== '') && (password1 === password1)) {
			setIsActive(true)
		} else {
			setIsActive(false)
		}
	}, [email, password1])
	return(
		(user.id !== null) ?
			<Navigate to="/courses"/>
		:
		<Fragment>
		<h1 className="mt-4">Login</h1>
		<Form onSubmit={(e) => loginUser(e)}>
			<Form.Group controlId="loginEmail">
				<Form.Label>
					Email Address
				</Form.Label>
				<Form.Control

				type='email' placeholder='Enter your email here' value={email} onChange={e => setEmail(e.target.value)} required/>
				<Form.Text className='text-muted'>
					We will never share your email with anyone else.
				</Form.Text>
			</Form.Group>
			<Form.Group controlId="passwordLogin">
				<Form.Label>Password</Form.Label>
				<Form.Control type='password' placeholder= 'Enter your password here' value={password1} onChange={e => setPassword1(e.target.value)} required/>
			</Form.Group>

			{ isActive ?
				<Button variant="success" type="submit" id="loginBtn" className="mt-3">
							Login
						</Button>

				:

				<Button variant="danger" type="submit" id="loginBtn" className="mt-3 mb-3" disabled>
							Login
						</Button>
			}

		</Form>
	</Fragment>


		)
}