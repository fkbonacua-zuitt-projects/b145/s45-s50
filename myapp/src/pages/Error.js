import { Row, Col } from 'react-bootstrap'
import { Link } from 'react-router-dom'

export default function Error() {
	return (
		<Row>
			<Col className= "p-5">
				<h1>Zuitt Booking</h1>
				<h2>Page Not Found</h2>
				<p>
					Go back to <Link to="/">Home</Link>
				</p>
			</Col>
		</Row>
		)
}